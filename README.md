# Transformer Anomaly Monitoring

This project helps in real-time monitoring the voltage waveform of the transformer and detect if any anomalies occur

[Project Planning Sheet](https://docs.google.com/spreadsheets/d/1r6Pmx3jKFT__5a5TeQcnn-nX2RYdCXshrYZ-1UonHF4/edit?usp=sharing)

### Procedure

* Fork the project repository
* Take up the task from issue
* Complete the task in repective directory of your forked repository
* Create a merge request to that task
* Comment in respective issues regarding doubts   
