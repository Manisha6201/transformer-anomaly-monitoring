/**@file timestamp.c
 * @brief this program reads the timestamp
 * @author manisha kumari
**/

/*library files*/
#include<stdio.h>
#include<time.h>   /* library time which provide time delay function*/
int timestamp();  /*function declaration return type int*/
int seconds();

int main()  /*same as void main() but return type is integer*/
{
printf("seconds since january 1,1970=%d\n",timestamp());
}

void timestamp()
{
/*start main in function*/
/*time_t library contain a"class" name time_t*/
/*here "seconds" is an object of time_t */
time_t seconds;   

/*value of seconds is equal to time(NULL)*/
/*NULL is "0",time is a function,in which we pass null, time(NULL) is function in time.h*/
seconds=time(NULL);
return seconds;   /*returns value to main*/

}








