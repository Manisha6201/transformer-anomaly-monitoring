/** @file sensor setup
    @brief this will configure the hardware connection
    @sensor chosen-zmpt101b
    @ADC chosen-ADS1015-12 bit ADC
    @developement board chosen-rasberry pi 3
    these all are supported by shunya APIs
    **/

    /*tell shunya interfaces hardware connection by running command*/

$ sudo vi /etc/shunya/interfaces/config.yaml

/*telling Hardware connections to Shunya Interfaces via Sensor ID's and Connection ID's.

pin 3:[1.2]     /*this will configure that <Sensor 1>.<Sensor pin 2> is connected to pin 3 of rasberry pi 3.*/

![image](https://lh3.googleusercontent.com/OsmHEoXhIrtTpm2E6lRpCBm7TMPX7qFXe4IIONZjnS7hbkXCe6qKGLsnmGbgY7C08IR1aQ=s170)
