/**@file sensor_reading.c
   @brief this file read the analog vltage from the sensor
   @author manisha kumari
**/

/* header files */
#include <stdio.h>
#include <shunyaInterfaces.h>   /*library which gives access to shunya APIs*/

/*main function*/
int main(void)
{

/*initilize the library*/
ititLib();

/*read data from sensor*/
float voltage = getAdc(1);

/*returns 0 if program runs successfully*/
return 0;
}
//submit your code with comments and spacings here